package Schafe;

public class Schafe {

	// Deklaration der Instanzvariablen (Membervariablen, Instanzmerkmale)
	String name;
	byte alter;
	float wollMenge;

	// Konstruktor 1
	public Schafe()
	 {
	 System.out.println("Im parameterlosen Konstruktor 1.");
	 }

	// Konstruktor 2
	public Schafe(String name)
	 {
	 this.name = name;
	 System.out.println("Im Konstruktor 2.");
	 }

	// Konstruktor 3
	public Schafe(String name, byte alter, float wolle)
	 {
	 this(name); // verketteter Konstruktoraufruf muss als Erstes erfolgen
	 
	 this.alter = alter; // Unterscheidung von Instanzvariable und lokaler
	 // Variable mit Hilfe des this-Punktoperators
	 wollMenge = wolle;
	 System.out.println("Im Konstruktor 3.");
	 }

	public static void main(String[] args) {
		System.out.println("\n--- Instanziere objekt1 ---");
		Schafe objekt1 = new Schafe();
		System.out.println("\n--- Instanziere objekt2 ---");
		Schafe objekt2 = new Schafe("Othello");
		System.out.println("\n--- Instanziere objekt3 ---");
		Schafe objekt3 = new Schafe("Cloud", (byte) 4, 2.15F);
		System.out.println();
		objekt1.merkmaleAusgeben("objekt1");
		objekt2.merkmaleAusgeben("objekt2");
		objekt3.merkmaleAusgeben("objekt3");
	}

	public void merkmaleAusgeben(String objektName) {
		System.out.println("\n- Ausgabe der Merkmale von " + objektName + " -");
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter + " Jahre");
		System.out.println("Wollmenge: " + wollMenge + " m^2");
	}

}
