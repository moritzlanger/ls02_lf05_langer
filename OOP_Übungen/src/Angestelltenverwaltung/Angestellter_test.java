package Angestelltenverwaltung;

public class Angestellter_test {

	// Hauptprogramm
	public static void main(String[] args) {
		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter();
		Angestellter personx = new Angestellter();

		// Setzen der Attribute f�r die Angestellten
		ang1.setName("Meier");
		ang1.setGehalt(4500);
		ang2.setName("Petersen");
		ang2.setGehalt(6000);
		
		
		// Bildschirmausgaben
		System.out.println("Name: " + ang1.getName());
		System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
		System.out.println("Name: " + ang2.getName());
		System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
		
	}

}
