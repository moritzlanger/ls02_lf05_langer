package Rechteck;

import java.awt.Point;

public class Rechteck {
	private double seite1;
	private double seite2;

	public Rechteck(double seite1, double seite2)
	{
	  setSeite1(seite1);
	  setSeite2(seite2);
	}

	public void setSeite1(double seite1) {
		if (seite1 > 0)
			this.seite1 = seite1;
		else
			this.seite2 = 0;
	}

	public void setSeite2(double seite2) {
		if (seite2 > 0)
			this.seite2 = seite2;
		else
			this.seite2 = 0;
	}
	
	public double getSeite2() {
		return this.seite2;
	}

	public double getFlaeche() {
		return seite1*seite2;
	}
	
	public double getUmfang() {
		return 2*seite1 + 2*seite2;
	}




}
