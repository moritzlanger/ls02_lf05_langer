package Raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	
	ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	// Raumschiff
	public Raumschiff() {

	}

	public Raumschiff(String schiffsname, int photonenentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {

	}

	// Schiffsname
	public void setschiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getschiffsname() {
		return this.schiffsname;
	}

	// Photonentorpedos
	public void setphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getphotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	// energieversorgung
	public void setenergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getenergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	// schilde
	public void setschildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getschildeInProzent() {
		return this.schildeInProzent;
	}

	// H�lle
	public void sethuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int gethuelleInProzent() {
		return this.huelleInProzent;
	}

	// Lebenserhaltungssysteme
	public void setlebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getlebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	// Androiden
	public void setandroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getandroidenAnzahl() {
		return this.androidenAnzahl;
	}

	// Photonenentorpedo Schiessen
	public void photonentorpedoSchiessen(String schiffsname) {
		
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
			
		}else {
			
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(schiffsname);
		}
	}

	// Phaserkanone Schiessen
	public void phaserkanoneSchiessen(String schiffsname) {

		if (energieversorgungInProzent < 50) {
			
			nachrichtAnAlle("-=*Click*=-");
		}else {
			
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(schiffsname);
		}
	}
	
	// Treffer
	private void treffer(String schiffsname) {
		
		System.out.print(schiffsname + "wurde getroffen!");
	}
	
	// Nachricht an alle
	public void nachrichtAnAlle(String nachricht) {
		
		System.out.println("\n" + nachricht);
		
		broadcastKommunikator.add(nachricht);
		
	}
	
	//broadcastKommunikator ausgeben
	public void broadcastKommunikatorAusgeben() {
		
		System.out.println("\nBrodcast Kommunikator:");
		
		for (int i=0;i < broadcastKommunikator.size();i++)
		
		System.out.println(broadcastKommunikator.get(i));
		
	}
	
	
	
	
	// Ladung hinzuf�gen
	public void addLadung(Ladung ladung) {
		
		this.ladungsverzeichnis.add(ladung);

	}
	
	
	// Eintr�ge Logbuch zur�ckgeben
	
	// Photonentorpedos Laden
	
	// Reperatur durchf�hren
	
	// Raumschiff zustand
	
	// Ladeverzeichnis ausgeben
	public void ausgabeLadungsverzeichnis() {

		System.out.println("\nLadung des Raumschiffes:");
		
		
		for(int i=0;i < ladungsverzeichnis.size();i++) {
			System.out.println(ladungsverzeichnis.get(i).getbezeichnung() + " = " + ladungsverzeichnis.get(i).getmenge());
		}
		
		
		
	}
	
	
	// Ladeverzeichnis aufr�umen
	
	
	
	
	
	
	

}
