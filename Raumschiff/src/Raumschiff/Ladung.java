package Raumschiff;

public class Ladung {

	private String bezeichnung;
	private int menge;

	public Ladung() {
	}

	public Ladung(String Bezeichnung, int menge) {
		
		setbezeichnung(Bezeichnung);
		setmenge(menge);
		
	}

	public void setbezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public String getbezeichnung() {
		return this.bezeichnung;
	}

	
	public void setmenge(int menge) {
		this.menge = menge;
	}

	public int getmenge() {
		return this.menge;
	}

	

}
