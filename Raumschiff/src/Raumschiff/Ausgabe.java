package Raumschiff;

public class Ausgabe {

	public static void main(String[] args) {

		// Zust�nde des Raumschiffes
		Raumschiff Z1 = new Raumschiff();

		System.out.println("Zust�nde des Raumschiffes:\n");

		System.out.println("Raumschiffname: " + Z1.getschiffsname());
		System.out.println("Photonentorpedo Anzahl: " + Z1.getphotonentorpedoAnzahl());
		System.out.println("Energieversorgung In Prozent: " + Z1.getenergieversorgungInProzent() + "%");
		System.out.println("Schilde In Prozent: " + Z1.getschildeInProzent() + "%");
		System.out.println("Huelle In Prozent: " + Z1.gethuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme In Prozent: " + Z1.getlebenserhaltungssystemeInProzent() + "%");
		System.out.println("Androiden Anzahl: " + Z1.getandroidenAnzahl());
		
		
		
		Z1.addLadung(new Ladung("Test1", 2));
		Z1.addLadung(new Ladung("Test2", 5));
		Z1.addLadung(new Ladung("Test3", 4));
		
		
		
		// Ladungen des Raumschiffes:
		Z1.ausgabeLadungsverzeichnis();
		
		
		
		
		Z1.nachrichtAnAlle("Nachricht");
		Z1.nachrichtAnAlle("Nachricht 2");
		
		Z1.broadcastKommunikatorAusgeben();
		
		
	}
}
